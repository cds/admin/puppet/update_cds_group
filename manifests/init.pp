class update_cds_group (
    String $extra_cds_members,
    String $extra_controls_members,
    String $extra_groups,
    String $conffile,
    String $update_user_script,
) {
    file { "/etc/cron.daily/$update_user_script":
	ensure => file,
	owner => 'root',
	group => 'root',
	mode => '0755',
	source => "puppet:///modules/update_cds_group/etc/cron.daily/$update_user_script",
    }

    file { 'updatecdsgroup_conf':
	path => $conffile,
	ensure => file,
	owner => 'root',
	group => 'root',
	mode => '0644',
	content => epp('update_cds_group/update-cds-group.conf.epp', {
	    'extra_cds_members' => $extra_cds_members,
	    'extra_controls_members' => $extra_controls_members,
	    'extra_groups' => $extra_groups,
	}),
    }
}
